import { Component } from '@angular/core';
import { BaseResponse } from 'src/app/model/base-response.model';
import { MessageService } from 'src/app/service/message.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = 'ecomerce-front';
  baseResponse = new BaseResponse;
  welcome: string = '';
  version: string = '';

  constructor(private messageService: MessageService) {
  
  }
  /**
   * Se obtiene la version de la aplicacion
   */
  getVersionApp() {
    this.messageService.getVersion().subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      console.log(this.baseResponse);
      this.version = this.baseResponse.message as string;
    });
  }

  /**
   * Se obtiene el mensaje de bienvenida
   */
  getWelcomeMessage() {
    this.messageService.getWelcomeMessage().subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      console.log(this.baseResponse);
      this.welcome = this.baseResponse.message as string;
    });
  }

  /**
   * Se ejecuta al cargar el componente
   */
  ngAfterContentInit() {
    this.getWelcomeMessage();
    this.getVersionApp();
  }
}
