import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BaseResponse } from 'src/app/model/base-response.model';
import { ProviderModel } from 'src/app/model/provider.model';
import { ProviderService } from 'src/app/service/provider.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-create-provider',
  templateUrl: './create-provider.component.html',
  styleUrls: ['./create-provider.component.css'],
})
export class CreateProviderComponent {
  providerForm!: FormGroup;
  submitted = false;
  baseResponse = new BaseResponse;


  constructor(private providerServide: ProviderService, 
              private formBuilder: FormBuilder, 
              private router: Router,
              public dialogRef: MatDialogRef<CreateProviderComponent>) {
    this.createProviderForm();
  }
  createProviderForm() {
    this.providerForm = this.formBuilder.group({
      name: ['', Validators.required],
      socialReason: ['', Validators.required],
      address: ['', Validators.required]
    })
  }

  /**
 * Metodo para registrar a una persona
 */
  validateForm(): void {
    this.submitted = true;
    if (this.providerForm.invalid) {
      return;
    } else {
      let providerModel: ProviderModel;
      providerModel = {
        id: 0,
        name: this.providerForm.value.name,
        socialReason: this.providerForm.value.socialReason,
        address: this.providerForm.value.address,
      }
      this.providerServide.createProvider(providerModel).subscribe(data => {
        this.baseResponse = (data as BaseResponse);
        Swal.fire({
          icon: 'success',
          title: this.baseResponse.message
        }).then(() =>     this.dialogRef.close(true))
      }, (error => {

        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: error.error.message,
        });
      }));
    }

  }

  /**
* Se obtienen los controles del formulario de personas
*/
  get dataForm(): any {
    return this.providerForm.controls;
  }
}
