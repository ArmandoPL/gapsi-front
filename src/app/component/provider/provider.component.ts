import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BaseResponse } from 'src/app/model/base-response.model';
import { ProviderModel } from 'src/app/model/provider.model';
import { ProviderService } from 'src/app/service/provider.service';
import { CreateProviderComponent } from './create-provider/create-provider.component';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent {

  providers!: ProviderModel[];
  baseResponse = new BaseResponse;
  displayedColumns: string[] = ['id', 'name', 'socialreason', 'address', 'actions'];
  items = Array.from({length: 100000}).map((_, i) => `Item #${i}`);

  constructor(private providerService: ProviderService, public dialog: MatDialog) {

  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateProviderComponent);

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      if (result) {
        this.getProviders();
      }
    });
  }

  /**
  * Se ejecuta al cargar el componente
  */
  ngAfterContentInit() {
    this.getProviders();
  }

  /**
   * Se obtiene lista de proveedores
   */
  getProviders() {
    this.providerService.getListProvider().subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      this.providers = this.baseResponse.responseData as ProviderModel[];
      console.log(this.providers);

    }, (error) => {
      this.providers = [];
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error.error.message,
      })
    });
  }

  /**
   * Eliminar proveedor
   * @param idProvider id del proveedor
   */
  deleteProvider(idProvider: number): void {
    Swal.fire({
      title: '¿Eliminar Proveedor?',
      text: "Esta acción no se podrá revertir!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6pri',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar'
    }).then((result) => {
      if (result.isConfirmed) {
        this.providerService.deleteProvider(idProvider).subscribe();
        setTimeout(() => {
          Swal.fire(
            'Eliminado!',
            'El proveedor ha sido eliminado.',
            'success'
          ).then(() => this.getProviders());
        }, 100);
      }
    })


  }

}
