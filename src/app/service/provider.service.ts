import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientResponseService } from './http-client-response.service';
import { Observable } from 'rxjs';
import { BaseResponse } from '../model/base-response.model';
import { ProviderModel } from '../model/provider.model';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  apiEndPoint = 'http://localhost:8080/api/provider';

  constructor(private httpClient: HttpClient, private httpResponseService: HttpClientResponseService) { }

  /**
   * Obtiene listado de proveedores
   * @returns lista de proveedores
   */
  public getListProvider(): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se registra un nuevo proveedor
   * @param provider Objeto a registrar del proveedor
   * @returns Objeto de proveedor cread
   */
  public createProvider(provider: ProviderModel): Observable<BaseResponse> {
    return this.httpClient.post<BaseResponse>(this.apiEndPoint, provider,
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Se elimina un proovedor
   * @param idProvider id del proovedor a eliminar
   * @returns 
   */
  public deleteProvider(idProvider: number): Observable<BaseResponse> {
    console.log(idProvider);
    
    return this.httpClient.delete<BaseResponse>(`${this.apiEndPoint}?idProvider=${idProvider}`,
      { headers: this.httpResponseService.cabeceras() });
  }
}
