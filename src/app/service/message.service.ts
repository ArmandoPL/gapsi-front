import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { HttpClientResponseService } from './http-client-response.service';
import { Observable } from 'rxjs';
import { BaseResponse } from '../model/base-response.model';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  apiEndPoint = 'http://localhost:8080/api';

  constructor(private httpClient: HttpClient, private httpResponseService: HttpClientResponseService) { }

  /**
   * Obtiene el mensaje de bienvenida a la aplicacion
   * @returns Mensaje de bienvenida
   */
  public getWelcomeMessage(): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + '/message/welcome/',
      { headers: this.httpResponseService.cabeceras() });
  }

  /**
   * Obtiene version de la aplicacion
   * @returns version de la app
   */
  public getVersion(): Observable<BaseResponse> {
    return this.httpClient.get<BaseResponse>(this.apiEndPoint + '/message/version/',
      { headers: this.httpResponseService.cabeceras() });
  }
}
