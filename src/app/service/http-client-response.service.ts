import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpClientResponseService {

  constructor() { }

  extractDataPost(res: Response): Promise<any> {
    const body = res.json();
      console.log('extractDataPost Body', body);
    return body;
  }

  handleError(error: Response | any): void {
    console.error(error.message || error);
  }

  cabeceras(): HttpHeaders {
    const headers: HttpHeaders = new HttpHeaders()
      .set('Content-Type', 'application/json;charset=UTF-8');
    return headers;

  }

  cabecerasAuth(): HttpHeaders {
    const headers: HttpHeaders = new HttpHeaders();
    // headers.append();
    // headers.append('authorization', 'Bearer ' + this.localStorageService.getToken());
    return headers;

  }


}
