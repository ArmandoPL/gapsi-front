import { BaseModel } from './base.model';

export class BaseResponse {
	
	private _code!: number;
	
	private _message!: string;
	
	private _responseData!: any;

	get code(): number {
		return this._code;
	}

	set code(_code: number) {
		this._code = _code;
	}

	get message(): string {
		return this._message;
	}

	set message(_message: string) {
		this._message = _message;
	}

	get responseData(): BaseModel {
		return this._responseData;
	}

	set responseData(_responseData: BaseModel) {
		this._responseData = _responseData;
	}


}