export interface ProviderModel {

    id: number;
    name?: string;
    socialReason?: string;
    address?: string;

    
}