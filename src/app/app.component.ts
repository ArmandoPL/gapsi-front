import { AfterContentInit, Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { MessageService } from './service/message.service';
import { BaseResponse } from './model/base-response.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  baseResponse = new BaseResponse;
  welcome: string = '';
  version: string = '';

  constructor(private messageService: MessageService) {
    this.getVersionApp();
  }
  /**
 * Se obtiene la version de la aplicacion
 */
  getVersionApp() {
    this.messageService.getVersion().subscribe(data => {
      this.baseResponse = (data as BaseResponse);
      console.log(this.baseResponse);
      this.version = this.baseResponse.message as string;
    });
  }
}
