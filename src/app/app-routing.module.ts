import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { CreateProviderComponent } from './component/provider/create-provider/create-provider.component';
import { ProviderComponent } from './component/provider/provider.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'provider',
    pathMatch: 'full',
    component: ProviderComponent
  },
  {
    path: 'create-provider',
    pathMatch: 'full',
    component: CreateProviderComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
